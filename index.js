const canvasWidth = 1024;
const canvasHeight = 768;

const playerWidth = 100;
const playerThickness = 20;
const player = 100;
const playerElevation = 100;

const ballSize = 20;
const ballSpeed = 10;
let ballPosX, ballPosY;

function setup() {
    createCanvas(canvasWidth, canvasHeight);
    background(0);
    frameRate(30);
    ballPosX = 25;
    ballPosY = 25;
}

function drawCeiling() {
    fill(188);
    rect(0, 0, canvasWidth, playerThickness);  
}

function updatePlayer() {
    fill(255);
    let posX = mouseX;

    if ((mouseX + (playerWidth / 2)) >= canvasWidth){
        posX = canvasWidth - (playerWidth / 2)
    } else if ((mouseX - (playerWidth /2)) <= 0) {
        posX = 0 + (playerWidth / 2)
    }
    rect(posX - (playerWidth / 2), canvasHeight - playerElevation, playerWidth, playerThickness);  
}

function updateBall() {
    fill(255);
    newPosX = ballPosX + ballSpeed
    newPosY = ballPosY + ballSpeed
    console.log(newPosX, newPosY)
    ellipse(newPosX, newPosY, ballSize, ballSize);
    ballPosX = newPosX
    ballPosY = newPosY
}

function checkForCollisions() {
    // TODO: Collision ball-player
    // TODO: Collision ball-ceiling
}

function checkGameStatus() {
    // TODO: Check if ball below player. Lose and restart
}

function draw() {
    background(0);
    drawCeiling()
    updateBall()
    updatePlayer()
    checkForCollisions()
    checkGameStatus()
}